#!/usr/bin/env python

from distutils.core import setup

setup(name='DG_PY_FOFBLegacy',
      version='2.1',
      description='Offer a similar API to Matlab FOFB/SOFB scripts when using new flavour platform.',
      url='https://gitlab.synchrotron-soleil.fr/dg/ds_dg_pytango_package/dg_py_fofblegacy',
      license="GNU-GPL-v3",
      author='Romain BRONÈS',
      author_email='romain.brones@synchrotron-soleil.fr',
      py_modules=["DG_PY_FOFBLegacy",],
      data_files=[
          ('',['DG_PY_FOFBLegacy',]),
          ],
      install_requires = ['pytango>=9.2', 'FofbTool>=2.2'],
     )

# NOTE we use data_files to ship the launcher, because of install on the secured nfs.
# We want the launcher on the root dir when deploy with pip -t /nfs/.../secured/../DG
