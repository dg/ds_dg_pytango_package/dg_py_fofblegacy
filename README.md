# DG_PY_FofbLegacy

## About this device

This device is used to offer a similar API to Matlab FOFB/SOFB scripts while moving the FOFB application to its new platform and control.
The new FOFB uses two main Tango devices: FofbCommand and FofbWatcher.

The use of the previous device Fofb-Manager is fading, but some features are still used.
Command for the BPM dedicated network synchronization and start are used by expert.
The FofbLegacy also forward arrays of attributes that the Fofb-Manager collects: BpmCnt, SpsIsOn...

This device skeleton is generated with Pogo, in PythonHL mode.
When possible, forwarded attributes have been used.


## Device overview

### Properties

| Property Name | Description |
| ------------- |-------------|
| FofbCommand | Tango path to the FofbCommand device |
| FofbWatcher | Tango path to the FofbWatcher device |
| FofbManager | Tango path to the Fofb-Manager device |

### Attributes

| Attribute Name | Type | Description |
| -------------- |------|-------------|
| [xz]RefOrbitLoadedOnDevice | Scalar Boolean RO | Simply check that the local attribute is not empty/zeros |
| [xz]RefOrbitLoadedOnBpms | Scalar Boolean RO | Translated to checking the date of attribute in FofbCommand and the loading date recorded in a attribute by FofbCommand |
| [xz]InvRespMatrixLoadedOnDevice | Scalar Boolean RO | Simply check that the local attribute is not empty/zeros |
| [xz]InvRespMatrixLoadedOnBpms | Scalar Boolean RO | Translated to checking the date of attribute in FofbCommand and the loading date recorded in a attribute by FofbCommand |
| [xz]SpsIsLiberaControlled | Spectrum Boolean RO | This attribute is forwarded from Fofb-Manager |
| [xz]SpsIsOn | Spectrum Boolean RO | This attribute is forwarded from Fofb-Manager |
| bpmCnt | Spectrum DevLong RO | This attribute is forwarded from Fofb-Manager |
| [xz]InvRespMatrix | Image DevDouble RW | This attribute is reshaped and forwarded to/from FofbCommand |
| [xz]FofbRunning | _Forwarded_ Scalar Boolean RO | Native Tango Forward (FofbWatcher) |
| [xz]RefOrbit | _Forwarded_ Spectrum DevLong RW | Native Tango Forward (FofbCommand) |
| applyCmdsTo[XZ]Plane | Boolean RW | Command Start/Stop are applied to X/Z plane. |

### Commands

All commands take no argument and return nothing.

| Command Name | Description |
| ------------ |-------------|
| StopToZero | Forwarded to FofbCommand |
| Stop | Forwarded to FofbCommand |
| ColdStart | This command is translated to:<br>- Check that SOFB is not running (-> Service-Locker-Matlab)<br>- Perform StopToZero (-> FofbLegacy)<br>- Switch on and set LiberaControl on steerers (-> FofbCommand)<br>- HotStart (-> FofbLegacy)<br><br>If sequence is interrupted by a failed check, return and display a Warning log and status.
| HotStart | This command is translated to:<br>- Check that SOFB is not running (-> Service-Locker-Matlab)<br>- Check that RefOrbit and InvMatrix are loaded (-> Fofb-Legacy)<br>- Start (-> FofbCommand)<br><br>If sequence is interrupted by a failed check, return and display a Warning log and status. |
| AcknowledgeError | Does nothing |
| StartStep0[45]Load[XY]RefOrbit | Forwarded to FofbCommand |
| StartStep0[67]Load[XY]InvRespMatrix | Forwarded to FofbCommand |


| Expert Command Name | Description |
| ------------------- |-------------|
| zeroRefLibera | Set orbit reference in Libera to zero. |
| StopComSteererLibera | Stop the all Libera serial outputs to steerers |
| StopComSteererCellNode | Stop the frame sending from cellnodes to all steerers. |
| StopComFofbNodes | This stops the communication on FofbNodes: in between them, from BPM and to PSC. |
| ConfFofbNodes | This applies default configuration on FofbNodes |
| StartComFofbNodes | This starts the communication of FofbNodes. |

