# -*- coding: utf-8 -*-
#
# This file is part of the DG_PY_FOFBLegacy project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" FOFBLegacy

This device is used for backward compatibility for the new FOFB Control (FOFBWatcher and FOFBControl).

It gives access to Attributes and Commands offered previously by FOFB-Manager
"""

# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
# Additional import
#----- PROTECTED REGION ID(DG_PY_FOFBLegacy.additionnal_import) ENABLED START -----#
import datetime
import time
import numpy as np
#----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.additionnal_import

__all__ = ["DG_PY_FOFBLegacy", "main"]


class DG_PY_FOFBLegacy(Device):
    """
    This device is used for backward compatibility for the new FOFB Control (FOFBWatcher and FOFBControl).
    
    It gives access to Attributes and Commands offered previously by FOFB-Manager

    **Properties:**

    - Device Property
        FofbCommand
            - Tango path to the FofbCommand device
            - Type:'DevString'
        FofbWatcher
            - Tango path to the FofbWatcher device
            - Type:'DevString'
        FofbManager
            - Tango path to the Fofb-Manager device.
            - Type:'DevString'
    """
    # PROTECTED REGION ID(DG_PY_FOFBLegacy.class_variable) ENABLED START #

    d_status = {}

    """
    def dev_status(self):
        return my_status()
    """

    def my_status(self):
        try:
            return "\n\n".join([str(v) for v in self.d_status.values()])+"\n"
        except Exception:
            self.debug_stream(str(self.d_status))
            return "Status failure\n"

    # PROTECTED REGION END #    //  DG_PY_FOFBLegacy.class_variable

    # -----------------
    # Device Properties
    # -----------------

    FofbCommand = device_property(
        dtype='DevString',
        mandatory=True
    )

    FofbWatcher = device_property(
        dtype='DevString',
        mandatory=True
    )

    FofbManager = device_property(
        dtype='DevString',
        mandatory=True
    )

    # ----------
    # Attributes
    # ----------

    xRefOrbitLoadedOnDevice = attribute(
        dtype='DevBoolean',
        doc="Simply check that the local attribute is not empty/zeros",
    )

    xRefOrbitLoadedOnBpms = attribute(
        dtype='DevBoolean',
        doc="Translated to checking the date of attribute in FofbCommand and the loading date recorded in a attribute by FofbCommand",
    )

    zRefOrbitLoadedOnDevice = attribute(
        dtype='DevBoolean',
        doc="Simply check that the local attribute is not empty/zeros",
    )

    zRefOrbitLoadedOnBpms = attribute(
        dtype='DevBoolean',
        doc="Translated to checking the date of attribute in FofbCommand and the loading date recorded in a attribute by FofbCommand",
    )

    xInvRespMatrixLoadedOnDevice = attribute(
        dtype='DevBoolean',
        doc="Simply check that the local attribute is not empty/zeros",
    )

    xInvRespMatrixLoadedOnBpms = attribute(
        dtype='DevBoolean',
        doc="Translated to checking the date of attribute in FofbCommand and the loading date recorded in a attribute by FofbCommand",
    )

    zInvRespMatrixLoadedOnDevice = attribute(
        dtype='DevBoolean',
        doc="Simply check that the local attribute is not empty/zeros",
    )

    zInvRespMatrixLoadedOnBpms = attribute(
        dtype='DevBoolean',
        doc="Translated to checking the date of attribute in FofbCommand and the loading date recorded in a attribute by FofbCommand",
    )

    applyCmdsToXPlane = attribute(
        dtype='DevBoolean',
        access=AttrWriteType.READ_WRITE,
        doc="Command Start/Stop are applied to X plane.",
    )

    applyCmdsToZPlane = attribute(
        dtype='DevBoolean',
        access=AttrWriteType.READ_WRITE,
        doc="Command Start/Stop are applied to Z plane.",
    )

    xSpsIsLiberaControlled = attribute(
        dtype=('DevBoolean',),
        max_dim_x=128,
        doc="This attribute is forwarded from Fofb-Manager",
    )

    zSpsIsLiberaControlled = attribute(
        dtype=('DevBoolean',),
        max_dim_x=128,
        doc="This attribute is forwarded from Fofb-Manager",
    )

    xSpsIsOn = attribute(
        dtype=('DevBoolean',),
        max_dim_x=128,
        doc="This attribute is forwarded from Fofb-Manager",
    )

    zSpsIsOn = attribute(
        dtype=('DevBoolean',),
        max_dim_x=128,
        doc="This attribute is forwarded from Fofb-Manager",
    )

    bpmCnt = attribute(
        dtype=('DevLong',),
        max_dim_x=512,
        doc="This attribute is forwarded from Fofb-Manager",
    )

    xRefOrbit = attribute(
        dtype=('DevDouble',),
        access=AttrWriteType.READ_WRITE,
        max_dim_x=512,
    )

    zRefOrbit = attribute(
        dtype=('DevDouble',),
        access=AttrWriteType.READ_WRITE,
        max_dim_x=512,
    )

    xInvRespMatrix = attribute(
        dtype=(('DevDouble',),),
        access=AttrWriteType.READ_WRITE,
        max_dim_x=512, max_dim_y=512,
        doc="This attribute is reshaped and forwarded to/from FofbCommand",
    )

    zInvRespMatrix = attribute(
        dtype=(('DevDouble',),),
        access=AttrWriteType.READ_WRITE,
        max_dim_x=512, max_dim_y=512,
        doc="This attribute is reshaped and forwarded to/from FofbCommand",
    )

    zFofbRunning = attribute(
        name="zFofbRunning",
        label="zFofbRunning",
        forwarded=True
    )
    xFofbRunning = attribute(
        name="xFofbRunning",
        label="xFofbRunning",
        forwarded=True
    )
    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the DG_PY_FOFBLegacy."""
        Device.init_device(self)
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.init_device) ENABLED START -----#
        self.set_state(tango.DevState.INIT)

        # Create and init member _apply_cmds_to_x_plane only if it doesnt exist (first init at startup)
        # Keeps the value for latter init
        if not hasattr(self, "_apply_cmds_to_x_plane"):
            self._apply_cmds_to_x_plane = False
            self._apply_cmds_to_z_plane = False

        try:
            self.d_status.pop('init')
        except KeyError:
            pass

        try:
            self.prx_fofbcommand = tango.DeviceProxy(self.FofbCommand)
            self.prx_fofbwatcher = tango.DeviceProxy(self.FofbWatcher)
            self.prx_fofbmanager = tango.DeviceProxy(self.FofbManager)
        except Exception as e:
            self.d_status["init"] = "Error at init"
            self.error_stream("Error at init:\n{}".format(str(e)))
            self.set_state(tango.DevState.FAULT)
            return


        self.info_stream("Init done !")
        self.set_state(tango.DevState.ON)
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.always_executed_hook) ENABLED START -----#
        state = tango.DevState.ON

        for n,p in zip(
                (self.FofbCommand, self.FofbWatcher, self.FofbManager),
                (self.prx_fofbcommand, self.prx_fofbwatcher, self.prx_fofbmanager)):

            try:
                try:
                    self.d_status.pop(n)
                except KeyError:
                    pass

                p.ping()

            except tango.DevFailed as e:
                self.error_stream("Cannot ping {}:\n{}".format(n, str(e)))
                self.d_status[n]="Cannot ping {}".format(n)
                state=tango.DevState.FAULT


            self.set_state(state)
            self.set_status(self.my_status())

        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.delete_device) ENABLED START -----#
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.delete_device
    # ------------------
    # Attributes methods
    # ------------------

    def read_xRefOrbitLoadedOnDevice(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.xRefOrbitLoadedOnDevice_read) ENABLED START -----#attr.set_value(self.attr_xRefOrbitLoadedOnDevice_read)
        # Simply check that the attribute is not empty/zeroes
        try:
            attr = self.prx_fofbcommand["x_ref_orbit"]
        except Exception:
            return False

        if len(attr.value) < 1:
            return False

        if np.all(attr.value == 0):
            return False

        return True
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.xRefOrbitLoadedOnDevice_read

    def read_xRefOrbitLoadedOnBpms(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.xRefOrbitLoadedOnBpms_read) ENABLED START -----#attr.set_value(self.attr_xRefOrbitLoadedOnBpms_read)
        # Use the loading time and attribute time
        try:
            attr_date = self.prx_fofbcommand["x_ref_orbit"].time.todatetime()
        except Exception:
            return False

        load_date = datetime.datetime.strptime(self.prx_fofbcommand.last_x_ref_orbit_loading_time, "%Y-%m-%d %H:%M:%S")
        return load_date - attr_date > datetime.timedelta(seconds=-1)
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.xRefOrbitLoadedOnBpms_read

    def read_zRefOrbitLoadedOnDevice(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.zRefOrbitLoadedOnDevice_read) ENABLED START -----#attr.set_value(self.attr_zRefOrbitLoadedOnDevice_read)
        # Simply check that the attribute is not empty/zeroes
        try:
            attr = self.prx_fofbcommand["y_ref_orbit"]
        except Exception:
            return False

        if len(attr.value) < 1:
            return False

        if np.all(attr.value == 0):
            return False

        return True
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.zRefOrbitLoadedOnDevice_read

    def read_zRefOrbitLoadedOnBpms(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.zRefOrbitLoadedOnBpms_read) ENABLED START -----#attr.set_value(self.attr_zRefOrbitLoadedOnBpms_read)
        # Use the loading time and attribute time
        try:
            attr_date = self.prx_fofbcommand["y_ref_orbit"].time.todatetime()
        except Exception:
            return False

        load_date = datetime.datetime.strptime(self.prx_fofbcommand.last_y_ref_orbit_loading_time, "%Y-%m-%d %H:%M:%S")
        return load_date - attr_date > datetime.timedelta(seconds=-1)
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.zRefOrbitLoadedOnBpms_read

    def read_xInvRespMatrixLoadedOnDevice(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.xInvRespMatrixLoadedOnDevice_read) ENABLED START -----#attr.set_value(self.attr_xInvRespMatrixLoadedOnDevice_read)
        # Simply check that the attribute is not empty/zeroes
        try:
            # This throw a DevFailed when attribute empty, handle it
            attr = self.prx_fofbcommand["x_inv_resp_mat"]
        except Exception:
            return False

        if len(attr.value) < 1:
            return False

        if np.all(attr.value == 0):
            return False

        return True
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.xInvRespMatrixLoadedOnDevice_read

    def read_xInvRespMatrixLoadedOnBpms(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.xInvRespMatrixLoadedOnBpms_read) ENABLED START -----#attr.set_value(self.attr_xInvRespMatrixLoadedOnBpms_read)
        # Use the loading time and attribute time
        try:
            # This throw a DevFailed when attribute empty, handle it
            attr_date = self.prx_fofbcommand["x_inv_resp_mat"].time.todatetime()
        except Exception:
            return False

        load_date = datetime.datetime.strptime(self.prx_fofbcommand.last_x_inv_resp_mat_loading_time, "%Y-%m-%d %H:%M:%S")
        return load_date - attr_date > datetime.timedelta(seconds=-1)
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.xInvRespMatrixLoadedOnBpms_read

    def read_zInvRespMatrixLoadedOnDevice(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.zInvRespMatrixLoadedOnDevice_read) ENABLED START -----#attr.set_value(self.attr_zInvRespMatrixLoadedOnDevice_read)
        # Simply check that the attribute is not empty/zeroes
        try:
            # This throw a DevFailed when attribute empty, handle it
            attr = self.prx_fofbcommand["y_inv_resp_mat"]
        except Exception:
            return False

        if len(attr.value) < 1:
            return False

        if np.all(attr.value == 0):
            return False

        return True
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.zInvRespMatrixLoadedOnDevice_read

    def read_zInvRespMatrixLoadedOnBpms(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.zInvRespMatrixLoadedOnBpms_read) ENABLED START -----#attr.set_value(self.attr_zInvRespMatrixLoadedOnBpms_read)
        # Use the loading time and attribute time
        try:
            # This throw a DevFailed when attribute empty, handle it
            attr_date = self.prx_fofbcommand["y_inv_resp_mat"].time.todatetime()
        except Exception:
            return False

        load_date = datetime.datetime.strptime(self.prx_fofbcommand.last_y_inv_resp_mat_loading_time, "%Y-%m-%d %H:%M:%S")
        return load_date - attr_date > datetime.timedelta(seconds=-1)
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.zInvRespMatrixLoadedOnBpms_read

    def read_applyCmdsToXPlane(self):
        # PROTECTED REGION ID(DG_PY_FOFBLegacy.applyCmdsToXPlane_read) ENABLED START #
        """Return the applyCmdsToXPlane attribute."""
        return self._apply_cmds_to_x_plane
        # PROTECTED REGION END #    //  DG_PY_FOFBLegacy.applyCmdsToXPlane_read

    def write_applyCmdsToXPlane(self, value):
        # PROTECTED REGION ID(DG_PY_FOFBLegacy.applyCmdsToXPlane_write) ENABLED START #
        """Set the applyCmdsToXPlane attribute."""
        self._apply_cmds_to_x_plane = value
        # PROTECTED REGION END #    //  DG_PY_FOFBLegacy.applyCmdsToXPlane_write

    def read_applyCmdsToZPlane(self):
        # PROTECTED REGION ID(DG_PY_FOFBLegacy.applyCmdsToZPlane_read) ENABLED START #
        """Return the applyCmdsToZPlane attribute."""
        return self._apply_cmds_to_z_plane
        # PROTECTED REGION END #    //  DG_PY_FOFBLegacy.applyCmdsToZPlane_read

    def write_applyCmdsToZPlane(self, value):
        # PROTECTED REGION ID(DG_PY_FOFBLegacy.applyCmdsToZPlane_write) ENABLED START #
        """Set the applyCmdsToZPlane attribute."""
        self._apply_cmds_to_z_plane = value
        # PROTECTED REGION END #    //  DG_PY_FOFBLegacy.applyCmdsToZPlane_write

    def read_xSpsIsLiberaControlled(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.xSpsIsLiberaControlled_read) ENABLED START -----#attr.set_value(self.attr_xSpsIsLiberaControlled_read)
        try:
            sps = self.prx_fofbmanager["xSpsIsLiberaControlled"].value
        except Exception:
            return [False,]

        return sps
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.xSpsIsLiberaControlled_read

    def read_zSpsIsLiberaControlled(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.zSpsIsLiberaControlled_read) ENABLED START -----#attr.set_value(self.attr_zSpsIsLiberaControlled_read)
        try:
            sps = self.prx_fofbmanager["zSpsIsLiberaControlled"].value
        except Exception:
            return [False,]

        return sps
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.zSpsIsLiberaControlled_read

    def read_xSpsIsOn(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.xSpsIsOn_read) ENABLED START -----#attr.set_value(self.attr_xSpsIsOn_read)
        try:
            sps = self.prx_fofbmanager["xSpsIsOn"].value
        except Exception:
            return [False,]

        return sps
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.xSpsIsOn_read

    def read_zSpsIsOn(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.zSpsIsOn_read) ENABLED START -----#attr.set_value(self.attr_zSpsIsOn_read)
        try:
            sps = self.prx_fofbmanager["zSpsIsOn"].value
        except Exception:
            return [False,]

        return sps

        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.zSpsIsOn_read

    def read_bpmCnt(self):
        # PROTECTED REGION ID(DG_PY_FOFBLegacy.bpmCnt_read) ENABLED START #
        try:
            bpmcnt = self.prx_fofbmanager["bpmcnt"].value
        except Exception:
            return [np.nan,]

        return bpmcnt

        # PROTECTED REGION END #    //  DG_PY_FOFBLegacy.bpmCnt_read

    def read_xRefOrbit(self):
        # PROTECTED REGION ID(DG_PY_FOFBLegacy.xRefOrbit_read) ENABLED START #
        """Return the xRefOrbit attribute."""
        try:
            ref = self.prx_fofbcommand["x_ref_orbit"].value
            self.debug_stream("Read x_ref_orbit from {} :\n{}".format(self.prx_fofbcommand.name(), str(ref)))
        except Exception as e:
            self.error_stream(str(e))
            return np.nan*np.ones(2)
        return ref*1e-6
        # PROTECTED REGION END #    //  DG_PY_FOFBLegacy.xRefOrbit_read

    def write_xRefOrbit(self, value):
        # PROTECTED REGION ID(DG_PY_FOFBLegacy.xRefOrbit_write) ENABLED START #
        """Set the xRefOrbit attribute."""
        try:
            self.prx_fofbcommand.x_ref_orbit = (value*1e6).astype('int')
        except Exception as e:
            self.error_stream("Failed to Write ref orbit:\n"+str(e))
        # PROTECTED REGION END #    //  DG_PY_FOFBLegacy.xRefOrbit_write

    def read_zRefOrbit(self):
        # PROTECTED REGION ID(DG_PY_FOFBLegacy.zRefOrbit_read) ENABLED START #
        """Return the zRefOrbit attribute."""
        try:
            ref = self.prx_fofbcommand["y_ref_orbit"].value
            self.debug_stream("Read y_ref_orbit from {} :\n{}".format(self.prx_fofbcommand.name(), str(ref)))
        except Exception:
            return np.nan*np.ones(2)
        return ref*1e-6
        # PROTECTED REGION END #    //  DG_PY_FOFBLegacy.zRefOrbit_read

    def write_zRefOrbit(self, value):
        # PROTECTED REGION ID(DG_PY_FOFBLegacy.zRefOrbit_write) ENABLED START #
        """Set the zRefOrbit attribute."""
        try:
            self.prx_fofbcommand.y_ref_orbit = (value*1e6).astype('int')
        except Exception as e:
            self.error_stream("Failed to Write ref orbit:\n"+str(e))
        # PROTECTED REGION END #    //  DG_PY_FOFBLegacy.zRefOrbit_write

    def read_xInvRespMatrix(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.xInvRespMatrix_read) ENABLED START -----#attr.set_value(self.attr_xInvRespMatrix_read)
        # Read from FofbCommand, reshape before forwarding
        try:
            mat = self.prx_fofbcommand["x_inv_resp_mat"].value
            self.debug_stream("Read x_inv_resp_mat from {} :\n{}".format(self.prx_fofbcommand.name(), str(mat)))
        except Exception:
            return np.nan*np.ones((1,1))
        return mat[:,1:123]
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.xInvRespMatrix_read

    def write_xInvRespMatrix(self, value):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.xInvRespMatrix_write) ENABLED START -----#
        # Reshape then forward
        try:
            self.prx_fofbcommand["x_inv_resp_mat"] = np.hstack([np.zeros((50, 1)), value, np.zeros((50,133))]).astype(int)
        except Exception:
            pass
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.xInvRespMatrix_write

    def read_zInvRespMatrix(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.zInvRespMatrix_read) ENABLED START -----#attr.set_value(self.attr_zInvRespMatrix_read)
        # Read from FofbCommand, reshape before forwarding
        try:
            mat = self.prx_fofbcommand["y_inv_resp_mat"].value
            self.debug_stream("Read y_inv_resp_mat from {} :\n{}".format(self.prx_fofbcommand.name(), str(mat)))
        except Exception:
            return np.nan*np.ones((1,1))
        return mat[:,1:123]
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.zInvRespMatrix_read

    def write_zInvRespMatrix(self, value):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.zInvRespMatrix_write) ENABLED START -----#
        # Reshape then forward
        try:
            self.prx_fofbcommand["y_inv_resp_mat"] = np.hstack([np.zeros((50, 1)), value, np.zeros((50,133))]).astype(int)
        except Exception:
            pass
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.zInvRespMatrix_write

    # --------
    # Commands
    # --------

    @command(
    )
    @DebugIt()
    def StopToZero(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.StopToZero) ENABLED START -----#
        try:
            if self._apply_cmds_to_x_plane:
                self.prx_fofbcommand.StopToZeroFofb_x()
                self.info_stream("Performed StopToZero() on X")
            if self._apply_cmds_to_z_plane:
                self.prx_fofbcommand.StopToZeroFofb_y()
                self.info_stream("Performed StopToZero() on Y")
        except Exception as e:
            self.error_stream("Failed to perform StopToZero()")
            self.error_stream(str(e))
        else:
            self.info_stream("Performed StopToZero()")
            self.d_status["start"] = "{} StopToZero() done.".format(datetime.datetime.now().strftime("%M%D-%H%M"))
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.StopToZero

    @command(
    )
    @DebugIt()
    def Stop(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.Stop) ENABLED START -----#
        try:
            if self._apply_cmds_to_x_plane:
                self.info_stream("Perform Stop() on X")
                self.prx_fofbcommand.StopFofb_x()
            if self._apply_cmds_to_z_plane:
                self.prx_fofbcommand.StopFofb_y()
                self.info_stream("Perform Stop() on Y")
        except Exception as e:
            self.error_stream("Failed to perform Stop()")
            self.error_stream(str(e))
        else:
            self.info_stream("Performed Stop()")
            self.d_status["start"] = "{} Stop() done.".format(datetime.datetime.now().strftime("%M%D-%H%M"))
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.Stop

    @command(
    )
    @DebugIt()
    def ColdStart(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.ColdStart) ENABLED START -----#
        try:
            if tango.AttributeProxy("ans/ca/service-locker-matlab/sofb").read().value:
                self.warning_stream("ColdStart() not performed because SOFB is running")
                self.d_status["start"] = "{} Start() blocked, SOFB is running.".format(datetime.datetime.now().strftime("%M%D-%H%M"))
                return

            self.StopToZero()
            self.prx_fofbcommand.StartSteerers()
            self.zeroRefLibera()

            time.sleep(4)

            self.HotStart()


        except Exception:
            self.error_stream("Failed to perform ColdStart()")
            self.d_status["start"] = "{} Start() failed.".format(datetime.datetime.now().strftime("%M%D-%H%M"))
        else:
            self.info_stream("Performed ColdStart()")

        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.ColdStart

    @command(
    )
    @DebugIt()
    def HotStart(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.HotStart) ENABLED START -----#
        try:
            if tango.AttributeProxy("ans/ca/service-locker-matlab/sofb").read().value:
                self.warning_stream("ColdStart() not performed because SOFB is running")
                self.d_status["start"] = "{} Start() blocked, SOFB is running.".format(datetime.datetime.now().strftime("%M%D-%H%M"))
                return

            if not (self.xRefOrbitLoadedOnBpms and self.zRefOrbitLoadedOnDevice and self.xInvRespMatrixLoadedOnBpms and self.zInvRespMatrixLoadedOnDevice):
                self.d_status["start"] = "{} Start() blocked, RefOrbit or InvMatrix not loaded.".format(datetime.datetime.now().strftime("%M%D-%H%M"))
                self.warning_stream("Start() not performed because either RefOrbit or InvMatrix is not loaded.")
                return

            if self._apply_cmds_to_x_plane:
                self.prx_fofbcommand.StartFofb_X()
                self.info_stream("Perform Start() on X")
            if self._apply_cmds_to_z_plane:
                self.prx_fofbcommand.StartFofb_Y()
                self.info_stream("Perform Start() on Y")

        except Exception as e:
            self.d_status["start"] = "{} Start() failed.".format(datetime.datetime.now().strftime("%M%D-%H%M"))
            self.error_stream("Failed to perform HotStart()")
            self.error_stream(str(e))
        else:
            self.d_status["start"] = "{} Start() done.".format(datetime.datetime.now().strftime("%M%D-%H%M"))
            self.info_stream("Performed HotStart()")

        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.HotStart

    @command(
    )
    @DebugIt()
    def AcknowledgeError(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.AcknowledgeError) ENABLED START -----#
        pass
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.AcknowledgeError

    @command(
    )
    @DebugIt()
    def StartStep04LoadXRefOrbit(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.StartStep04LoadXRefOrbit) ENABLED START -----#
        try:
            self.prx_fofbcommand.LoadRefOrbitX()
        except Exception:
            self.error_stream("Failed to LoadRefOrbitX()")
        else:
            self.info_stream("Performed LoadRefOrbitX()")

        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.StartStep04LoadXRefOrbit

    @command(
    )
    @DebugIt()
    def StartStep05LoadZRefOrbit(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.StartStep05LoadZRefOrbit) ENABLED START -----#
        try:
            self.prx_fofbcommand.LoadRefOrbitY()
        except Exception:
            self.error_stream("Failed to LoadRefOrbitY()")
        else:
            self.info_stream("Performed LoadRefOrbitY()")
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.StartStep05LoadZRefOrbit

    @command(
    )
    @DebugIt()
    def StartStep06LoadXInvRespMatrix(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.StartStep06LoadXInvRespMatrix) ENABLED START -----#
        try:
            self.prx_fofbcommand.LoadInvRespMatrixX()
        except Exception:
            self.error_stream("Failed to LoadInvRespMatrixX()")
        else:
            self.info_stream("Performed LoadInvRespMatrixX()")
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.StartStep06LoadXInvRespMatrix

    @command(
    )
    @DebugIt()
    def StartStep07LoadZInvRespMatrix(self):
        #----- PROTECTED REGION ID(DG_PY_FOFBLegacy.StartStep07LoadZInvRespMatrix) ENABLED START -----#
        try:
            self.prx_fofbcommand.LoadInvRespMatrixY()
        except Exception:
            self.error_stream("Failed to LoadInvRespMatrixY()")
        else:
            self.info_stream("Performed LoadInvRespMatrixY()")
        #----- PROTECTED REGION END -----#	//	DG_PY_FOFBLegacy.StartStep07LoadZInvRespMatrix

    @command(
        display_level=DispLevel.EXPERT,
    )
    @DebugIt()
    def zeroRefLibera(self):
        # PROTECTED REGION ID(DG_PY_FOFBLegacy.zeroRefLibera) ENABLED START #
        """

        :return:None
        """
        try:
            self.prx_fofbmanager.xreforbit = np.zeros(122)
            self.prx_fofbmanager.zreforbit = np.zeros(122)
            self.prx_fofbmanager.StartStep04LoadXRefOrbit()
            self.prx_fofbmanager.StartStep05LoadZRefOrbit()
        except Exception as e:
            self.error_stream(str(e))

        # PROTECTED REGION END #    //  DG_PY_FOFBLegacy.zeroRefLibera

    @command(
        display_level=DispLevel.EXPERT,
    )
    @DebugIt()
    def StopComSteererLibera(self):
        # PROTECTED REGION ID(DG_PY_FOFBLegacy.StopComSteererLibera) ENABLED START #
        """
        Stop the all Libera serial outputs to steerers

        :return:None
        """
        db = tango.Database()
        ansbpms = list(db.get_device_exported("ANS-C*/DG/BPM.*").value_string)
        ansbpms.remove("ANS-C13/DG/BPM.2-DMZ")
        ansbpms.remove("ANS-C13/DG/BPM.9-DMZ")
        for n in ansbpms:
            try:
                bpm = tango.DeviceProxy(n)
                bpm.writefadata([512*4-4, 4, 1, 0])
                bpm.writefadata([768*4-4, 4, 1, 0])
                bpm.writefadata([0x2000, 4, 1, 9])
                bpm.writefadata([0x2000, 4, 1, 8])
            except Exception as e:
                self.error_stream(str(e))

        # PROTECTED REGION END #    //  DG_PY_FOFBLegacy.StopComSteererLibera

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the DG_PY_FOFBLegacy module."""
    # PROTECTED REGION ID(DG_PY_FOFBLegacy.main) ENABLED START #
    return run((DG_PY_FOFBLegacy,), args=args, **kwargs)
    # PROTECTED REGION END #    //  DG_PY_FOFBLegacy.main


if __name__ == '__main__':
    main()
